import os
files = []
for i in os.listdir("."):
    if i.endswith('.txt'):
        files.append(i)
with open("contents.txt", "w") as out:
    helper = []
    for i in range(len(files)):
        with open(files[i], "r") as inputt:
            helper.append(inputt.read()+"\n")
    helper.sort()
    helper_str = "sort text by ABC:\n\n"
    for i in helper:
    	helper_str+=i+"\n"
    out.write(helper_str)


